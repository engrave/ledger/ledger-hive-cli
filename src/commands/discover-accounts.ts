import {Command, flags} from '@oclif/command'
import Transport from '@ledgerhq/hw-transport-node-hid-noevents'
import Hive from '@engrave/ledger-app-hive'
import cli from 'cli-ux'
import HiveConnector from '../utils/hive-connector'
import {Ledger} from '../utils/ledger'

enum Roles {
  owner = 0,
  active = 1,
  memo = 3,
  posting = 4
}

export function getStringValuesFromEnum<T>(myEnum: T): (keyof T)[] {
  return Object.keys(myEnum).filter(k => typeof (myEnum as any)[k] === 'number') as any
}

export default class DiscoverAccounts extends Command {
  static description = 'discover accounts associated with connected Ledger device'

  static examples = [
    `$ hive-ledger-cli discover-accounts
`,
  ]

  static flags = {
    help: flags.help({char: 'h'}),
    testnet: flags.boolean({char: 't', description: 'use testnet configuration'}),
    dry: flags.boolean({char: 'd', description: 'dry run will only print signed transaction instead broadcasting it'}),
    blind: flags.boolean({char: 'b', description: 'blind signing'}),
    'key-gap': flags.integer({
      char: 'k',
      description: 'Gap limit for unused key indexes on which the software decides that device is not used',
      default: 2,
    }),
    'account-gap': flags.integer({
      char: 'a',
      description: 'Gap limit for unused account indexes after which the software decides that device is not used',
      default: 2,
    }),
  }

  static args = [
    {
      name: 'role',
      required: false,
      description: 'Role to check for',
      options: getStringValuesFromEnum(Roles),
      default: Roles[Roles.owner],
    },
  ]

  async run() {
    const {args, flags} = this.parse(DiscoverAccounts)

    const client = HiveConnector.getClient(flags.testnet)

    const progressBar = cli.progress({
      format: 'Reading | {bar} | {value}/{total} keys',
      barCompleteChar: '\u2588',
      barIncompleteChar: '\u2591',
    })

    try {
      cli.action.start('Establishing transport with Hive application')
      const transport = await Transport.create()
      const hive = new Hive(transport)
      cli.action.stop()

      const keys: string[] = []
      const keyToPath = new Map()

      progressBar.start(flags['account-gap'] * flags['key-gap'])

      for (let account = 0; account < flags['account-gap']; account++) {
        for (let keyIndex = 0; keyIndex < flags['key-gap']; keyIndex++) {
          const path = `m/48'/13'/${Roles[args.role]}'/${account}'/${keyIndex}'`
          // eslint-disable-next-line no-await-in-loop
          const key = await hive.getPublicKey(path, false, HiveConnector.getAddressPrefix(flags.testnet))
          keys.push(key)
          keyToPath.set(key, path)
          progressBar.update((account * flags['key-gap']) + keyIndex + 1)
        }
      }

      progressBar.stop()
      cli.action.start('Searching for accounts')
      const {accounts} = await client.keys.getKeyReferences(keys)
      cli.action.stop()

      const data = accounts.map((multipleAccounts, index) => {
        if (multipleAccounts.length > 0) {
          return multipleAccounts.map((account => ({
            account: account,
            role: args.role,
            key: keys[index],
            path: keyToPath.get(keys[index]),
          })))
        }
        return undefined
      }).filter(Boolean).flat()

      if (data.length > 0) {
        this.log('\nAccounts associated with your device:\n')

        cli.table(data as any[], {
          account: {
            minWidth: 7,
          }, key: {
            minWidth: 7,
          }, role: {
            minWidth: 7,
          }, path: {
            minWidth: 7,
          },
        })
      } else {
        this.log('\nNo accounts associated with this device')
      }
    } catch (error) {
      Ledger.translateError(error)
    }
  }
}
