import {Command, flags} from '@oclif/command'
import Transport from '@ledgerhq/hw-transport-node-hid-noevents'
import Hive from '@engrave/ledger-app-hive'
import cli from 'cli-ux'
import {Ledger} from '../utils/ledger'
import HiveConnector from '../utils/hive-connector'

export default class GetPublicKey extends Command {
  static description = 'get public key derived from BIP32 (SLIP-0048) path'

  static examples = [
    `$ hive-ledger-cli get-public-key "m/48'/13'/0'/0'/0'"
Establishing transport with Hive application... done
STM5m57x4BXEePAzVNrjUqYeh9C2a7eez1Ya2wPo7ngWLQUdEXjKn
`,
  ]

  static flags = {
    help: flags.help({char: 'h'}),
    confirm: flags.boolean({char: 'c', description: 'Force confirmation on Ledger device'}),
    testnet: flags.boolean({char: 't', description: 'Use testnet configuration'}),
    blind: flags.boolean({char: 'b', description: 'blind signing'}),
  }

  static args = [{
    name: 'path',
    description: 'BIP 32 path to derive key from',
    required: true,
  }]

  async run() {
    const {args, flags} = this.parse(GetPublicKey)

    try {
      cli.action.start('Establishing transport with Hive application')
      const transport = await Transport.create()
      const hive = new Hive(transport)
      cli.action.stop()
      if (flags.confirm) {
        const publicKey = await hive.getPublicKey(args.path, false, HiveConnector.getAddressPrefix(flags.testnet))
        this.log(`Received public key: ${publicKey}`)
        cli.action.start('Please confirm public key on your ledger')
        await hive.getPublicKey(args.path, flags.confirm)
        cli.action.stop()
        this.log(publicKey)
      } else {
        const publicKey = await hive.getPublicKey(args.path, flags.confirm, HiveConnector.getAddressPrefix(flags.testnet))
        cli.action.stop()
        this.log(publicKey)
      }
    } catch (error) {
      Ledger.translateError(error)
    }
  }
}
