import {Command, flags} from '@oclif/command'
import HiveConnector from '../utils/hive-connector'
import {transactionHandler} from '../utils/transaction-handler'
import {Ledger} from '../utils/ledger'

export default class CancelTransferFromSavings extends Command {
  static description = 'cancel pending transfer from savings'

  static examples = [
    `$ hive-ledger-cli cancel-transfer-from-savings "m/48'/13'/0'/0'/0'" engrave 1
Establishing transport with Hive application... done
Review and confirm transaction on your device... done
Transaction 1f0c8376cf989483dbb9c3d5fa1006ded00cb6c5 broadcasted successfully

https://test.ausbit.dev/tx/1f0c8376cf989483dbb9c3d5fa1006ded00cb6c5`,
  ]

  static flags = {
    help: flags.help({char: 'h'}),
    testnet: flags.boolean({char: 't', description: 'use testnet configuration'}),
    dry: flags.boolean({char: 'd', description: 'dry run will only print signed transaction instead broadcasting it'}),
    blind: flags.boolean({char: 'b', description: 'blind signing'}),
  }

  static args = [
    {
      name: 'path',
      description: 'BIP 32 (SLIP-0048) path to derive key from and use to sign the transaction',
      required: true,
    },
    {
      name: 'from',
      description: 'account for which you want to cancel the request',
      required: true,
    },
    {
      name: 'requestId',
      description: 'request ID',
      required: true,
      parse: (input: string) => Number(input),
    },
  ]

  async run() {
    const {args, flags} = this.parse(CancelTransferFromSavings)

    try {
      const transaction = await HiveConnector.prepareTransaction([['cancel_transfer_from_savings', {
        from: args.from,
        request_id: args.requestId,
      }]], flags.testnet)
      await transactionHandler(this.log, args, flags, transaction)
    } catch (error) {
      Ledger.translateError(error)
    }
  }
}
