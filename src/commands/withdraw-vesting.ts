import {Command, flags} from '@oclif/command'
import HiveConnector from '../utils/hive-connector'
import {transactionHandler} from '../utils/transaction-handler'
import {Ledger} from '../utils/ledger'

export default class WithdrawVesting extends Command {
  static description = 'power down your Hive Power and convert it to liquid HIVE'

  static examples = [
    `$ hive-ledger-cli withdraw-vesting "m/48'/13'/0'/0'/0'" engrave "20.000000 VESTS"
Establishing transport with Hive application... done
Review and confirm transaction on your device... done
Transaction broadcasted successfully with id eb88f7c23cf6e1d183e7bfbd12e204906b33af69

https://hiveblockexplorer.com/tx/eb88f7c23cf6e1d183e7bfbd12e204906b33af69

`,
  ]

  static flags = {
    help: flags.help({char: 'h'}),
    testnet: flags.boolean({char: 't', description: 'use testnet configuration'}),
    dry: flags.boolean({char: 'd', description: 'dry run will only print signed transaction instead broadcasting it'}),
    blind: flags.boolean({char: 'b', description: 'blind signing'}),
  }

  static args = [
    {
      name: 'path',
      description: 'BIP 32 (SLIP-0048) path to derive key from and use to sign the transaction',
      required: true,
    },
    {
      name: 'account',
      description: 'account name to power down',
      required: true,
    },
    {
      name: 'vesting_shares',
      description: 'amount of VESTS to withdraw and convert to liquid HIVE',
      required: true,
    },
  ]

  async run() {
    const {args, flags} = this.parse(WithdrawVesting)

    try {
      const transaction = await HiveConnector.prepareTransaction([['withdraw_vesting', {
        account: args.account,
        vesting_shares: args.vesting_shares,
      }]], flags.testnet)
      await transactionHandler(this.log, args, flags, transaction)
    } catch (error) {
      Ledger.translateError(error)
    }
  }
}
