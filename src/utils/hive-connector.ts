// eslint-disable-next-line unicorn/filename-case
import {Client, Operation, SignedTransaction} from '@hiveio/dhive'

const testnetChainId = '18dcf0a285365fc58b71f18b3d3fec954aa0c141c44e4e5cb4cf777b9eab274e'

const testnetClient = new Client('https://testnet.openhive.network', {
  addressPrefix: 'TST',
  chainId: testnetChainId,
})

const mainnetClient = new Client('https://api.openhive.network')

export default class HiveConnector {
  public static testnetChainId = testnetChainId

  public static chainId = 'beeab0de00000000000000000000000000000000000000000000000000000000'

  public static async prepareTransaction(operations: Operation[], testnet = false) {
    const client = testnet ? testnetClient : mainnetClient
    const properties = await client.database.getDynamicGlobalProperties()
    return {
      ref_block_num: properties.head_block_number,
      ref_block_prefix: Buffer.from(properties.head_block_id, 'hex').readUInt32LE(4),
      expiration: new Date(Date.now() + (60 * 1000)).toISOString().slice(0, -5),
      operations,
      extensions: [],
    }
  }

  public static async broadcast(transaction: SignedTransaction, testnet = false) {
    const client = testnet ? testnetClient : mainnetClient
    return client.broadcast.send(transaction)
  }

  public static getChainId(testnet = false) {
    return testnet ? this.testnetChainId : this.chainId
  }

  public static getAddressPrefix(testnet = false) {
    return testnet ? 'TST' : 'STM'
  }

  public static getClient(testnet = false) {
    return testnet ? testnetClient : mainnetClient
  }
}
